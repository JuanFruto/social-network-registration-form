Non-functional Requirements: 
* Usability
* Accessibility
* Security
* Complete and unambiguous design

Functional Requirements:
* Users can easily understand how to register themselves 
* Tab navigation works right for non-visual people
* Content of "Password" and "Confirm Password" must be the same
* Password length must be larger than six characters
* CSS stored in external files and maps to HTML using classes