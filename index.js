function submitSingupForm() {
    return validations();
}

function validations() {
    return validatePassword();
}

function validatePassword() {
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirm-password').value;

    if(password != confirmPassword) {
        alert("Passwords do not match.");
        return false;
    }

    if(password <= 6) {
        alert("Passwords must be larger than 6 characters");
        return false;
    }

    console.log("password validations passed");
    return true;
}
